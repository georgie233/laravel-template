<?php
namespace Georgie\LaravelTemplate\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class CopyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'g:copy {type?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'copy Template';

    protected $module;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = ucfirst($this->argument('type'));
        $this->info("your type： ".$name);
        if ($name == 'Lang'){
            $this->copyLang();//复制中文语言包
            return $this->info("copy complete!");
        }
//        $this->module = ucfirst($this->argument('name'));

//        $this->setVars('',$this->module);
        if (\Module::has("Admin")) {
            $this->copyFiles($name == 'Y');//复制Admin下的模板文件
        }
        $this->copyPublicFiles();//复制public样式
    }


    //copy fun
    protected function xCopy($source, $destination, $child = 1){//用法：
        // xCopy("feiy","feiy2",1):拷贝feiy下的文件到 feiy2,包括子目录
        // xCopy("feiy","feiy2",0):拷贝feiy下的文件到 feiy2,不包括子目录
        //参数说明：
        // $source:源目录名
        // $destination:目的目录名
        // $child:复制时，是不是包含的子目录

        if(!is_dir($source)){
            echo("Error:the $source is not a direction!");
            return 0;
        }

        if(!is_dir($destination)){
            mkdir($destination,0777);
        }

        $handle=dir($source);
        while($entry=$handle->read()) {
            if(($entry!=".")&&($entry!="..")){
                if(is_dir($source."/".$entry)){
                    if($child)
                        $this->xCopy($source."/".$entry,$destination."/".$entry,$child);
                }
                else{
                    copy($source."/".$entry,$destination."/".$entry);
                }
            }
        }
        //return 1;
    }

    protected function copyFiles($bool = false)
    {
        if ($bool){
            //copy layouts
            $a = __DIR__.'/../dis/views/layouts';
            $b = \Module::getModulePath('Admin').'Resources/views/layouts';
            $this->xCopy($a,$b,1);
            return $this->info("copy complete!");
        }
        //copy Admin/网页模板文件
        $files = glob(__DIR__.'/../dis/views/layouts/*.php');
        foreach ($files as $file) {
            $to = \Module::getModulePath('Admin').'Resources/views/layouts/'.basename($file);
            if (is_file($to)) {
                $this->info($to." is exists");
                continue;
            }
            file_put_contents($to,$file);
            $this->info("{$to} file create successfully");
        }
    }

    protected function copyPublicFiles()
    {
        //copy public
        $a = __DIR__.'/../dis/public';
        $b = public_path('');
        $this->xCopy($a,$b,1);
    }

    protected function copyLang(){
        //copy lang
        $a = __DIR__.'/../dis/lang';
        $b = base_path('resources/lang');
        $this->xCopy($a,$b,1);
    }
}
