<?php

namespace Georgie\LaravelTemplate;

use Georgie\Module\Traits\ConfigService;

/**
 * Class Facade
 *
 * @package Georgie\Module
 */
class Provider
{
    use ConfigService, PermissionService, MenusService,ModuleService;
}
