<?php

namespace Georgie\LaravelTemplate;

use Georgie\LaravelTemplate\Commands\CopyCommand;
use Illuminate\Support\ServiceProvider;

class LaravelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        if ($this->app->runningInConsole()) {
            $this->commands([
                CopyCommand::class,
            ]);
        }


        $this->loadMigrationsFrom(__DIR__.'/Migrations');


        $path = base_path("Modules/Admin/Resources/views");
        //配置文件
        $this->publishes([
            __DIR__.'/dis/test.php' => public_path('aaa/test.php'),
        ]);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->singleton('GModule', function () {
//            return new Provider();
//        });
    }
}
