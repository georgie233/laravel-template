@foreach(['success','warning','danger'] as $t)
    @if(session()->has($t))
        <div class="alert-msg">
            <div role="alert" class="alert alert-{{$t}} alert-dismissible">
                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true"
                                                                                                  class="mdi mdi-close"></span>
                </button>
                <div class="icon">
                    @if($t == 'success')
                        <span class="mdi mdi-check"></span>
                    @else
                        <span class="mdi mdi-alert-triangle"></span>
                    @endif
                </div>
                <div class="message">
                    <strong>{{session()->get($t)}}</strong>
                </div>
            </div>
        </div>
    @endif
@endforeach

@if (session('status'))
    <div class="alert-msg">
        <div role="alert" class="alert alert-success alert-dismissible">
            <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true"
                                                                                              class="mdi mdi-close"></span>
            </button>
            <div class="icon"><span class="mdi mdi-check"></span></div>
            <div class="message">
                {{ session('status') }}
            </div>
        </div>
    </div>
@endif
<script type="text/javascript">
    //当前端有缓存过提示消息进行打印
    let dom = localStorage.getItem('dom');
    if (dom) document.write(dom);
    localStorage.setItem('dom', '');
</script>
