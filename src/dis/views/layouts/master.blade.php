<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title',\GModule::config('admin.config.title'))</title>
    <meta name="description" content="@yield('description')"/>
    <meta name="keywords" content="@yield('keywords')"/>
    <link rel="Shortcut Icon" type="image/x-icon" href="{{config('web.ico')}}">

    {{--  css  --}}
    <link href="{{url('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/beagle/lib/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/beagle/lib/material-design-icons/css/material-design-iconic-font.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('theme/beagle/css/app.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('theme/beagle/css/mobile.css')}}" type="text/css">

    {{--loading--}}
    <link rel="stylesheet" href="{{asset('theme/plugin/pjax.css')}}" type="text/css">

    {{--  js  --}}
    <script src="{{asset('theme/beagle/js/jquery3.min.js')}}" type="text/javascript"></script>


    @yield('style')
    @yield('top')
</head>
<body>
<div class="be-wrapper be-fixed-sidebar" id="VueApp">
    <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
            <div class="be-navbar-header" style="width: auto;">
                <a href="/" class="ml-2">
                    <img style="height: 60px;" class="p-1" src="{{\GModule::config('admin.config.ico')}}" alt="">
                </a>
                <a href="/" class="text-dark"
                   style="line-height: 60px;font-size: 18px;">{{\GModule::config('admin.config.title')}}</a>
            </div>
            <div class="be-right-navbar" style="width: auto;order:2;">
                <ul class="nav navbar-nav float-right be-user-nav">
                    <li class="nav-item dropdown">
                        <a href="#" data-toggle="dropdown" role="button" aria-expanded="false"
                           class="nav-link dropdown-toggle">
                            <img src="{{asset('theme/beagle/img/avatar.png')}}" alt="Avatar">
                            <span class="user-name">
                                {{ \Illuminate\Support\Facades\Auth::user()['nick_name']??'用户昵称' }}
                            </span>
                        </a>
                        <div role="menu" class="dropdown-menu">
                            <div class="user-info">
                                <div class="user-name">
                                    {{ \Illuminate\Support\Facades\Auth::user()['nick_name']??'用户昵称' }}
                                </div>
                                <div class="user-position online">
                                    {{ \Illuminate\Support\Facades\Auth::user()['name']??'用户名' }}
                                </div>
                            </div>
                            <a href="#" class="dropdown-item">
                                <span class="icon mdi mdi-face"></span> 修改密码
                            </a>
                            <a href="#" class="dropdown-item">
                                <span class="icon mdi mdi-settings"></span> 帐号设置
                            </a>
                            <a href="javascript:void(0);" class="dropdown-item"
                               onclick="event.preventDefault();document.getElementById('logout').submit()">
                                <span class="icon mdi mdi-power"></span> 退出
                            </a>
                            <form action="/logout" method="post" id="logout">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
            <a href="#" id="sub-navigation" data-toggle="collapse" aria-expanded="false"
               data-target="#be-navbar-collapse" class="g_menu be-toggle-top-header-menu collapsed">No Sidebar Left</a>
            <div id="be-navbar-collapse" class="g_menu navbar-collapse collapse">
            </div>
        </div>
    </nav>
    <div class="be-left-sidebar">
        <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">菜单栏</a>
            <div class="left-sidebar-spacer">
                @include('admin::layouts._menus')
            </div>
            <div class="progress-widget">
                <div class="progress-data">
                    <span class="name">
                        Georgie &copy; Copyright Reserved
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="be-content">
        <div class="main-content container-fluid">
            <div class="main-content container-fluid" id="pjax-container">
                <!--pjax加载动画-->
                <div id="loading">
                    <div class="spinner">
                        <div class="rect1"></div>
                        <div class="rect2"></div>
                        <div class="rect3"></div>
                        <div class="rect4"></div>
                        <div class="rect5"></div>
                    </div>
                </div>
                <!--pjax加载动画 结束-->
{{--                <div id="app">--}}
                    @include('admin::layouts._error')
                    @include('admin::layouts._message')
                    @yield('content_script')
                    <div id="app_content">
                        @yield('content')
                    </div>
                    @yield('scripts')
{{--                </div>--}}
                <script src="{{asset('theme/plugin/image.js')}}"></script>
            </div>

        </div>
    </div>
</div>
{{--  js  --}}

<script src="{{asset('theme/beagle/js/bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('js/app.js')}}" type="text/javascript"></script>
<script src="{{asset('theme/beagle/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('theme/beagle/js/app.js')}}" type="text/javascript"></script>
<script src="{{asset('theme/plugin/dialog.js')}}" type="text/javascript"></script>

{{-- vue --}}
<script src="{{asset('theme/plugin/vue.js')}}"></script>

{{--pjax--}}
<script src="{{asset('theme/plugin/jquery.pjax.js')}}" type="text/javascript"></script>
<script src="{{asset('theme/plugin/pjax.js')}}" type="text/javascript"></script>
<script src="{{asset('theme/plugin/menu.js')}}" type="text/javascript"></script>



<script type="text/javascript">
    $(document).ready(function () {
        //initialize the javascript
        App.init();
    });

    function IsPC() {
        const userAgentInfo = navigator.userAgent;
        const Agents = ["Android", "iPhone",
            "SymbianOS", "Windows Phone",
            "iPad", "iPod"];
        let flag = true;
        for (let v = 0; v < Agents.length; v++) {
            if (userAgentInfo.indexOf(Agents[v]) > 0) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    const flag = IsPC(); //true为PC端，false为手机端
    if (!flag) {
        $('.table-responsive-md').css({
            'display': 'block',
            'width': '100%',
            'overflow-x': 'auto',
        });
        $('.be-right-navbar').css({
            'width': '100vw'
        });
        $('.g_menu *').css({
            'max-width': '0px',
            'display': 'none',
            'overflow': 'hidden'
        });
        $('.g_menu').css({
            'max-width': '0px',
            'display': 'none',
            'overflow': 'hidden'
        });
    }
</script>
<style>
    .be-scroll-top{
        bottom: 54px !important;
    }
</style>


{{-- vue demo --}}
<script type="text/javascript">
    // $(function (){
    //     new Vue({
    //         el: '#app'
    //     })
    // })
</script>
<script type="text/javascript">
    function delNextForm(that) {
        $.showDialog({
            title: "确定删除吗?", type: "confirm", okButtonText: "删除", buttonColor: 'red', okCallback: function () {
                $(that).next('form').trigger('submit');
            }
        });
    }

    $(function () {
        $(document).on('click', 'table>tbody>tr>td>button.btn.btn-secondary.btn-danger', function () {
            delNextForm(this);
        });
        $(document).on('click', '.card>.card-body>.collapse>.card-footer>button.btn.btn-secondary.btn-danger', function () {
            delNextForm(this);
        });
    })
</script>
</body>
</html>
