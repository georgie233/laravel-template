@if(count($errors)>0)
    <div role="alert" class="alert alert-warning alert-simple alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="icon ml-1"><span class="mdi mdi-alert-triangle"></span></div>
        <div class="message">
            <ul class="p-0 m-0">
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
