$.extend({
    showDialog: function (obj) {
        var type = obj.type || "alert";
        var title = obj.title;
        var content = "<div id='pop-div' style='position: fixed; z-index:99999; top: 0; left: 0;width:100%; height:100%; background-color: rgba(0 ,0, 0, 0.5)'>";
        content += "<div style='position:relative; width: 90%; max-width: 400px; height: 140px;margin: 0 auto; top: 50%; -moz-transform: translateY(-50%); -ms-transform: translateY(-50%); -webkit-transform: translateY(-50%);transform: translateY(-50%);background-color: #fafafa;text-align: center;font-size: 16px'>";
        content += "<div style='height: 90px; border-bottom: 1px solid #E1E1E1;line-height: 90px; box-sizing: border-box'>" + title + "</div>";

        var button = "<div style='width: 100%; height: 40px;'>";
        var btnColor = obj.buttonColor ?? 'orange';
        switch (type) {
            case "confirm":
                var okButtonText = obj.okButtonText || "确定";
                var cancleButtonText = obj.cancleButtonText || "取消";
                var button = "<span style='display:inline-block; width: 50%;height: 50px;border-right: 1px solid #E1E1E1; box-sizing: border-box;text-align: center;line-height: 50px;font-size: 16px;cursor: pointer;' id='cancle'>" + cancleButtonText + "</span><span style='display:inline-block; width: 50%;height: 50px;text-align: center;line-height: 50px; color: " + btnColor + ";font-size: 16px;cursor: pointer;' id='confirm'>" + okButtonText + "</span>";
                break;
            case "alert":
            default:
                var buttonText = obj.buttonText || "确定";
                var button = "<span style='display:inline-block; width: 100%;line-height: 50px;color: " + btnColor + ";font-size: 16px;cursor: pointer;' id='alert'>" + buttonText + "</span>";
                break;
        }
        button += "</div>";
        content += button;
        content += "</div>";
        content += "</div>";
        $("body").append(content);

        if (type == 'confirm') {
            var okCallback = obj.okCallback || undefined;
            var cancleCallback = obj.cancleCallback || undefined;
            $("#confirm").on("click", function () {
                $("#pop-div").remove();
                if (typeof okCallback == "function") {
                    okCallback();
                }
            });
            $("#cancle").on("click", function () {
                $("#pop-div").remove();
                if (typeof cancleCallback == "function") {
                    cancleCallback();
                }
            });
        } else if (type == 'alert') {
            var callback = obj.callback || undefined;
            $("#alert").click(function () {
                $("#pop-div").remove();
                if (typeof callback == 'function') {
                    callback();
                }
            });
        }
    }
});
