//定义加载区域
$(document).pjax('a:not([not])','#pjax-container');
//定义pjax有效时间，超过这个时间会整页刷新
$.pjax.defaults.timeout = 4000;
//显示加载动画
$(document).on('pjax:click', function() {
    $("#loading").show();
});
//隐藏加载动画
$(document).on('pjax:end', function() {
    $("#loading").hide();
    imgInit();//重新初始化图片上传功能
});
