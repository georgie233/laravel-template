# jQuery实用插件
### jQuery弹出框工具
``` javascript
/**
     * 弹出框
     * {type: "alert", title: "title", buttonText: "buttonText", callback: "callback"}
     * {type: "confirm", title: "title", okButtonText: "okButtonText", cancleButtonText: "cancleButtonText", okCallback: "okCallback", cancleCallback: "cancleCallback"}
     */

/*
type：类型，可为alert或者confirm
title： 提示框内容
type为alert时可选参数：
buttonText:按钮内容，可选，默认为确定
buttonColor:按钮字体颜色，可选，默认为orange
callback:点击俺就调用函数，可选

type为confirm时可选参数：
okButtonText: 确认按钮文字，默认确定，可选
cancleButtonText：取消按钮文字，默认取消，可选
okCallback：点击确认按钮触发事件，可选
cancleCallback: 点击取消按钮触发事件，可选
*/
```
``` javascript
    $.showDialog({
        title: "这只是一个测试", type: "confirm", okButtonText: "点我", buttonTitle: "", okCallback: function () {
            alert("123");
        }
    });
```